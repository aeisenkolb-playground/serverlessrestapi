# E4 Serverless API

Very basic Serverless REST API containing an API Gateway with a NodeJS Lambda integration.

## Dependencies

- `AWS Account`
- `Docker`  (https://docs.docker.com/get-docker/)
- `AWS CLI` (https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
- `AWS SAM` (https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)
- `AWS CDK` (https://docs.aws.amazon.com/cdk/v2/guide/getting_started.html)

## Local testing

Unfortunately, the AWS CDK does not yet have build in mechanism to test lambda functions locally.
However, in combination with AWS SAM it is easily achievable (see: [https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-cdk-testing.html](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-cdk-testing.html)).

The following commands can be used for local testing:

* `npm run invoke:lambda:get`    Invokes the lambda function with a mocked GET request (./events/get-event.json)
* `npm run invoke:lambda:post`   Invokes the lambda function with a mocked POST request (./events/post-event.json)
* `npm run start:api`            Spins up a local API Gateway in front of the lambda (http://127.0.0.1:3000/)

## Useful commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `cdk deploy`      deploy this stack to your default AWS account/region
* `cdk diff`        compare deployed stack with current state
* `cdk synth`       emits the synthesized CloudFormation template
* `cdk destroy`     destroys the stack
