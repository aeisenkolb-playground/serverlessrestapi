import { CfnOutput, Stack, StackProps } from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { NodejsFunction } from 'aws-cdk-lib/aws-lambda-nodejs';
import { LambdaIntegration, RestApi } from 'aws-cdk-lib/aws-apigateway';

export class ApiStack extends Stack {
    constructor(scope: Construct, id: string, props?: StackProps) {
        super(scope, id, props);

        // VPC settings, reserved concurrency etc. would go here, but is omitted to simplify the use case
        const handler = new NodejsFunction(this, 'handler', { functionName: 'Function' });

        // Usage plan setting, default CORS handling etc. is also omitted for the use case
        const api = new RestApi(this, 'Rest-Api', {
            defaultIntegration: new LambdaIntegration(handler),
        });

        api.root.addMethod('GET');
        api.root.addMethod('POST');

        new CfnOutput(this, 'Rest-Api-URL', {
            value: api.url,
        });
    }
}
