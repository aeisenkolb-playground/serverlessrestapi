import { APIGatewayEvent } from 'aws-lambda';

export async function handler(event: APIGatewayEvent) {
    const { username, password } = JSON.parse(event.body || '{}');
    const contentType = event.headers[ 'Content-Type' ] || '';


    let template = `
            Welcome to our demo API, here are the details of your request:
            
            ***Headers***
            Content-Type: ${contentType}
            
            ***Method***
            ${event.httpMethod}
    `;

    if (event.httpMethod === 'POST') {
        template += `
            Body:
            {"username":"${username}","password:"${password}"}
        `;
    }

    console.log(template);

    return {
        statusCode: 204,
    };
}
