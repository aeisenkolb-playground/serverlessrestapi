import * as cdk from 'aws-cdk-lib';
import { Template } from 'aws-cdk-lib/assertions';
import * as Api from '../lib/api-stack';


test('RestApi created', () => {
    const app = new cdk.App();
    // WHEN
    const stack = new Api.ApiStack(app, 'MyTestStack');
    // THEN
    const template = Template.fromStack(stack);

    template.hasResourceProperties('AWS::ApiGateway::RestApi', {
        Name: 'Rest-Api',
    });
});

test('Lambda created', () => {
    const app = new cdk.App();
    // WHEN
    const stack = new Api.ApiStack(app, 'MyTestStack');
    // THEN
    const template = Template.fromStack(stack);

    template.hasResourceProperties('AWS::Lambda::Function', {
        FunctionName: 'Function',
        Runtime: 'nodejs14.x',
    });
});
